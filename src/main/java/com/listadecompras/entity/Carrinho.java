package com.listadecompras.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity (name = "Carrinho")
public class Carrinho {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer carrinhoId;
	
	@OneToOne (fetch = FetchType.LAZY, mappedBy = "carrinho")
	private Cliente cliente;
	
	@OneToMany (cascade = CascadeType.ALL)
	private List<Produto> produtos;
	
	

	public Integer getCarrinhoId() {
		return carrinhoId;
	}

	public void setCarrinhoId(Integer carrinhoId) {
		this.carrinhoId = carrinhoId;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
}
