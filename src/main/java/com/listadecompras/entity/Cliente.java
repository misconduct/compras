package com.listadecompras.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.listadecompras.entity.Carrinho;

@Entity(name = "Cliente")
@Table(name = "Cliente")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	// Cadastro
	//----------------------
	//Nome
	@Column(nullable = false)
	private String nome;
	
	//senha ??
	
	//Email
	@Column(nullable = false)
	private String email;
	
	// CPF (000.000.000.00 - 11 numeros)
	@Column(nullable = false)
	private long cpf;
	
	// Nascimento (dd/mm/yyyy ou dd/mm/yy && mm/dd/yyyy ou mm/dd/yy - 8 ou 6 numeros)
	@Column(nullable = false)
	private long nascimento;

	// Cliente criado auto generated Carrinho
	
	@Column(nullable = false)
	private String address;
	
	@Column(nullable = true)
	 private String latitude;
	 
	@Column(nullable = true)
	 private String longitude;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "carrinhoId")
	@JsonIgnore
	private Carrinho carrinho;
	

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Carrinho getCarrinho() {
		return carrinho;
	}

	public void setCarrinho(Carrinho carrinho) {
		this.carrinho = carrinho;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public long getCpf() {
		return cpf;
	}

	public void setCpf(long cpf) {
		this.cpf = cpf;
	}

	public long getNascimento() {
		return nascimento;
	}

	public void setNascimento(long nascimento) {
		this.nascimento = nascimento;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

}