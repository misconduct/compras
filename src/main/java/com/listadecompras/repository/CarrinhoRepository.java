package com.listadecompras.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.listadecompras.entity.Carrinho;

@Repository
public interface CarrinhoRepository extends CrudRepository<Carrinho, Integer>{
	
	
}
