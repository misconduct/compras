package com.listadecompras.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.listadecompras.entity.Produto;

@Repository
public interface ProdutoRepository extends CrudRepository<Produto, Integer>{
	
}