package com.listadecompras.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.listadecompras.entity.Cliente;

@Repository
public interface ClienteRepository extends CrudRepository<Cliente, Integer>{
	

}
