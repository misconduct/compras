package com.listadecompras.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.listadecompras.entity.Cliente;
import com.listadecompras.entity.Produto;
import com.listadecompras.exception.RecordNotFoundException;
import com.listadecompras.repository.ClienteRepository;
import com.listadecompras.repository.ProdutoRepository;

@Service
public class ClienteService {

	// FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA//FOCA
	@Autowired
	private ProdutoRepository prodRep;
	@Autowired
	private ClienteRepository cliRep;

	// Mostrar Lista
	public List<Produto> findAll() {
		return (List<Produto>) prodRep.findAll();
	}
	// Mostrar Lista
		public List<Cliente> showAll() {
			List<Cliente> cliList = new ArrayList<Cliente>();
			cliRep.findAll().forEach(e -> cliList.add(e));
			return cliList;
		}

	// Adicionar
	public Iterable<Cliente> carregar(List<Cliente> clientes) {
		return cliRep.saveAll(clientes);
	}
	
	// Delete
		public void deletetar(Integer id) throws RecordNotFoundException{
			
			if (prodRep.findById(id).isPresent()) {
				prodRep.deleteById(id);
			} else {
				throw new RecordNotFoundException("Product Not Found");
			}
		}
		
		// Delete
				public void delite(Integer id) throws RecordNotFoundException{
					
					if (cliRep.findById(id).isPresent()) {
						cliRep.deleteById(id);
					} else {
						throw new RecordNotFoundException("Client Not Found");
					}
				}
		
		// Pegando 
		public Cliente findById(@PathVariable("id") Integer id) {
			if (cliRep.findById((int) id).isPresent()) {
				return cliRep.findById((int) id).get();
			}
			return null;
		}
}
