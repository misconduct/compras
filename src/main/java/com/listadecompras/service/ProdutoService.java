package com.listadecompras.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import com.listadecompras.entity.Produto;
import com.listadecompras.exception.RecordNotFoundException;
import com.listadecompras.repository.ProdutoRepository;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository prodRep;

	// Pegando Item
	public Produto getById(@PathVariable("id") Integer id) {
		if (prodRep.findById(id).isPresent()) {
			return prodRep.findById(id).get();
		}
		return null;
	}

	// Mostrar Lista
	public List<Produto> findAll() {
		List<Produto> prodList = new ArrayList<Produto>();
		prodRep.findAll().forEach(e -> prodList.add(e));
		return prodList;
	}

	// Adicionar
	public Iterable<Produto> carregar(List<Produto> produtos) {
		return prodRep.saveAll(produtos);
	}

	// Delete
	public void deletetar(Integer id) throws RecordNotFoundException{
		
		if (prodRep.findById(id).isPresent()) {
			prodRep.deleteById(id);
		} else {
			throw new RecordNotFoundException("Product Not Found");
		}
	}
}