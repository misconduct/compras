package com.listadecompras.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class OperationForbidenException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public OperationForbidenException(String message) {
        super(message);
    }
     
    public OperationForbidenException(String message, Throwable t) {
    	super(message, t);
    }
}
