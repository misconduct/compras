package com.listadecompras.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.OK)
public class RecordNotFoundException extends Exception{
	
	private static final long serialVersionUID = 1L;
	
	public RecordNotFoundException(String message) {
        super(message);
    }
     
    public RecordNotFoundException(String message, Throwable t) {
    	super(message, t);
    }
}
