package com.listadecompras.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.listadecompras.service.CarrinhoService;

@RestController
public class CarrinhoController {

	@Autowired
	private CarrinhoService cartServ;
	
	@GetMapping ("/carrinho/teste")
	public List<String> listAll(){
	return Arrays.asList("Sistem", "Looks Good");
	}
	@PostMapping ("/carrinho/remove")
	public ResponseEntity<Boolean> remove(){
		HttpStatus status = cartServ.excluir(0);
		return new ResponseEntity<>(true, status);
	}
}