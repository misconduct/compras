package com.listadecompras.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.listadecompras.entity.Cliente;
import com.listadecompras.entity.Produto;
import com.listadecompras.exception.RecordNotFoundException;
import com.listadecompras.service.ClienteService;
import com.listadecompras.service.ProdutoService;

@RestController
public class ClienteController {

	@Autowired
	private ProdutoService prodServ;
	@Autowired
	private ClienteService cliServ;
	
	
	@GetMapping ("/clientes/produtos/{id}")
	public ResponseEntity<Produto> getById(@PathVariable ("id") Integer id){
	if (prodServ.getById(id) != null) {
		return new ResponseEntity<>(prodServ.getById(id), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@GetMapping("/clientes/admin/{id}")
	public ResponseEntity<Cliente> findById(@PathVariable("id") Integer id) {
		Cliente cliente = cliServ.findById(id);
		if (cliente != null) {
			return new ResponseEntity<>(cliente, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@PostMapping("/clientes")
	public Iterable<Cliente> adicionar(@RequestBody List<Cliente> clientes) {
		return cliServ.carregar(clientes);
	}
	
	@GetMapping("/clientes/admin")
	public ResponseEntity<List<Cliente>> showAll() {
		return new ResponseEntity<>(cliServ.showAll(), HttpStatus.OK);
	}
	
	@DeleteMapping("/clientes/carrinhos/produtos/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Integer id) throws RecordNotFoundException{
		cliServ.deletetar(id);
		return new ResponseEntity<String>("Delete Sucess", HttpStatus.OK);
	}
	
	@DeleteMapping("/clientes/admin/{id}")
	public ResponseEntity<String> delite(@PathVariable("id") Integer id) throws RecordNotFoundException{
		cliServ.delite(id);
		return new ResponseEntity<String>("Delete Sucess", HttpStatus.OK);
	}
	
}