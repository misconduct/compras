package com.listadecompras.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.listadecompras.entity.Produto;
import com.listadecompras.exception.RecordNotFoundException;
import com.listadecompras.service.ProdutoService;

@RestController
public class ProdutoController {

	@Autowired
	private ProdutoService prodServ;	

	@GetMapping("/produtos/{id}")
	public ResponseEntity<Produto> getById(@PathVariable("id") Integer id) {
		Produto produto = prodServ.getById(id);
		if (produto != null) {
			return new ResponseEntity<>(produto, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@GetMapping("/produtos")
	public ResponseEntity<List<Produto>> findAll() {
		return new ResponseEntity<>(prodServ.findAll(), HttpStatus.OK);
	}

	@PostMapping("/produtos")
	public Iterable<Produto> adicionar(@RequestBody List<Produto> produtos) {
		return prodServ.carregar(produtos);
	}

	@DeleteMapping("/produtos/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Integer id) throws RecordNotFoundException{
		prodServ.deletetar(id);
		return new ResponseEntity<String>("Delete Sucess", HttpStatus.OK);
	}
		
}
